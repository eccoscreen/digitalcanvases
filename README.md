# canvases

canvases is a real-time previsualization developed in TouchDesigner for a set of 6 LED panels controlled by DMX signalling. It can pull in any visual media whether it’s generative, video, or images and display it on the virtual LED panels in a pixelized fashion which map directly to each box. 

More details at [eccoscreen.xyz/canvases](https://eccoscreen.xyz/canvases)